# การสร้าง Docker image และขึ้น Docker (Vue.js)
การสร้างโปรแจคบน Docker นั้นมีหลายวิธี แต่ในที่นี้จะใช้วิธีการ สร้าง Docker ที่ Build File dist ของ vue.js แล้วนำไฟล์ img push ขึ้น Registry ในที่นี้จะใช้ Registry ของ Gitlab จะประกอบไปด้วย 4 ขั้นตอนหลักคือ 
- การเตรียมไฟล์ที่ใช้งาน
- การ build Docker image
- การ Push Docker image เข้า Registry
- การเรียกใช้งาน Docker image

ก่อนที่จะเริ่มต้น เครื่องคอมพิวเตอร์จะต้อง ติดตั้ง และ Run Docker สามารถตรวจสอบได้โดยการใช้คำสั่ง
```sh
docker -v
```
### 1.การเตรียมไฟล์ที่ใช้งาน
ในการเตียมไฟล์นี้จะมี 3 ไฟล์ที่สำคัญอยู่ 3 ส่วนได้แก่ 
- ไฟล์โปรเจคที่สามารถใช้งานได้
- ไฟล์ Dockerfile
- ไฟล์ nginx.conf

ไฟล์ Dockerfile เป็นไฟล์ที่เรานั้นจะใช้ในการสร้าง Docker Image ภายในประกอบด้วย
###### Dockerfile
```
# Stage 0, based on Node.js, to build and compile Angular
FROM node:10.4.1-alpine as node
WORKDIR /app
COPY package*.json /app/
RUN npm install
COPY ./ /app/
RUN npm run build

# Stage 1, based on Nginx, to have only the compiled app, ready for production with Nginx
FROM nginx:1.14.0-alpine
COPY --from=node /app/dist/ /usr/share/nginx/html
COPY ./nginx.conf /etc/nginx/conf.d/default.conf
```
ไฟล์ nginx.conf เป็นไฟล์ที่ใช้ในการ Config ตัว nginx ที่ใช้สำหรับเป็น Web Server ภายในประกอยด้วย
###### nginx.conf
```
server {
    listen 80;
    access_log off;
    error_log off;
    location / {
        root /usr/share/nginx/html;
        index index.html index.htm;
        try_files $uri $uri/ /index.html =404;
    }
}
```
### 2.การ build Docker image
เป็นการสร้าง Docker Image จากไฟล์ Docker 
###### การดู Docker Image
```sh
docker images
```
###### การ ลบ Docker image
```sh
docker rmi [image id]
```
###### การ Build Docker Image ( ดูได้จาก Registry ของ Gitlab )
```sh
docker build -t git.inet.co.th:5555/ukrit.fo/example_docker:v1.0 .
```
หาก Build แล้วเกิด Error 
```sh
npm ERR! code ENOGIT
npm ERR! Error while executing:
npm ERR! undefined ls-remote -h -t ssh://git@github.com/eligrey/FileSaver.js.git
npm ERR! 
npm ERR! undefined
npm ERR! No git binary found in $PATH
npm ERR! 
npm ERR! Failed using git.
npm ERR! Please check if you have git installed and in your PATH.

npm ERR! A complete log of this run can be found in:
npm ERR!     /root/.npm/_logs/2019-07-12T06_29_24_282Z-debug.log
```
ทำการเพิ่ม
```sh
RUN apk add --no-cache git
```
เนื่องจาก ตัว Node ที่ทำการ pull ลงมานั้นไม่มี Git ติดมาด้วยจึงต้องทำการ Install Git ลงมา [Link](https://github.com/nodejs/docker-node/issues/586)

### 3.การ Push Docker image เข้า Registry
```sh
docker push git.inet.co.th:5555/ukrit.fo/example_docker:v1.0
```
เมื่อทำการ Push Image เสร็จเรียร้อยแล้ว ตัว Image ของเราจะจะถูก Upload เข้าสู่ Registry ของ Gitlab เรียร้อย รอการนำไปใช้งาน
### 4.การเรียกใช้งาน Docker image(Server ที่ต้องการ Run Project)
ในส่วนของการนำ Docker Image ที่เรา Upload ไว้ใน Registry นั้นสามารถเรียกได้โดยเรียกชื่อของ Image นั้นๆ แต่เพื่อให้ง่ายต่อการนำไปใช้งาน เราจึงนิยมสร้างไฟล์ docker-compose.yml เพื่อทำการ pull Docker Image , Config และ ทำการ Run Container 
###### docker-compose.yml
```sh
version: "3"

services:
  web:
    image: git.inet.co.th:5555/ukrit.fo/example_docker:v1.0
    ports:
      - 80:80
    restart: always
```
###### คำสั่งในการ Run Container
```sh
docker-compose up 
or
docker-compose up -d
```
###### คำสั่งที่ใช้ในการ ตรวจสอบ Container 
```sh
docker ps -a
```
###### คำสั่งที่ใช้ในการ หยุด Container(container จะไม่ถูกลบแต่จะหยุดทำงาน)  
```sh
docker stop [container id]
```
###### คำสั่งที่ใช้ในการ หยุดและลบ container
```sh
docker-compose down
```
###### คำสั่งที่ใช้ในการดู Log ( เมื่อ Run container แบบ Background  -d)
```sh
docker logs -f [container id]
```
###### การดูข้อมูลใน image ทำได้โดยการใช้คำสั่ง
```sh
docker exec -it [container id] bash
```
##### คำสั่งในการเคลีย Container
```sh
docker system prune -a
```


